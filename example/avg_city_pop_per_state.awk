# Dataset: http://media.mongodb.org/zips.json
# In MongoDB:
# db.zips.aggregate([{"$group":{"_id":{"state":"$state","city":"$city"},"pop":{"$sum":"$pop"}}},{"$group":{"_id":"$_id.state", "avgCityPop":{"$avg":"$pop"}}}])

BEGIN {
    JPAT[1] = "/state"
    JPAT[2] = "/city"
    JPAT[3] = "/pop"
}
{ pop[J[1]][J[2]] += J[3] }
END {
    for (state in pop) {
        for (city in pop[state]) {
            pop2[state]["pop"] += pop[state][city]
            pop2[state]["count"] += 1
        }
    }
    for (state in pop2) {
        print state, pop2[state]["pop"] / pop2[state]["count"]
    }
}
