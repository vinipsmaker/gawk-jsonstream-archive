# Dataset: http://media.mongodb.org/zips.json
# In MongoDB:
# db.zips.aggregate([{"$match":{"state":"NY"}},{"$group":{"_id":"$city","population":{"$sum":"$pop"}}},{"$project":{"_id":0,"city":"$_id","population":1}},{"$sort":{"population":-1}}])

BEGIN {
    JPAT[1] = "/state"
    JPAT[2] = "/city"
    JPAT[3] = "/pop"
}
J[1] != "NY" { next }
{ pop[J[2]] += J[3] }
END {
    PROCINFO["sorted_in"] = "@val_num_desc"
    for (city in pop) {
        print city, pop[city]
    }
}
