# Dataset: http://media.mongodb.org/zips.json
# In MongoDB:
# db.zips.aggregate([{"$group":{"_id":"$state", "pop":{"$max":"$pop"}}}])

BEGIN {
    JPAT[1] = "/state"
    JPAT[2] = "/pop"
}
!(J[1] in pop) || J[2] > pop[J[1]] {
    pop[J[1]] = J[2]
}
END {
    for (state in pop) {
        print state, pop[state]
    }
}
