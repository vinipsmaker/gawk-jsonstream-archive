# Dataset: http://media.mongodb.org/zips.json
# In MongoDB:
# db.zips.aggregate([{"$group":{"_id":"$state", "averapeg_pop":{"$avg":"$pop"}}}])

BEGIN {
    JPAT[1] = "/state"
    JPAT[2] = "/pop"
}
{
    pop[J[1]]["pop"] += J[2]
    pop[J[1]]["count"] += 1
}
END {
    for (state in pop) {
        print state, pop[state]["pop"] / pop[state]["count"]
    }
}
