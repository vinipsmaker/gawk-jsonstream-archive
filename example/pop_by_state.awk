# Dataset: http://media.mongodb.org/zips.json
# In MongoDB:
# db.zips.aggregate([{"$group":{"_id":"$state", "population":{"$sum":"$pop"}}}])

BEGIN {
    JPAT[1] = "/state"
    JPAT[2] = "/pop"
}
{ pop[J[1]] += J[2] }
END {
    for (state in pop) {
        print state, pop[state]
    }
}
