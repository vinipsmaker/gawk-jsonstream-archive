extern "C" {
#include <unistd.h>
}

#include <string_view>
#include <charconv>
#include <cassert>
#include <variant>
#include <string>
#include <vector>
#include <map>

#include <boost/hana.hpp>

#include <trial/protocol/json/partial/skip.hpp>
#include <trial/protocol/json/reader.hpp>

// Must be included before gawkapi {{{
#include <cstdio>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <cstdlib>

extern "C" {
#include <sys/stat.h>
}

using std::FILE;
using std::memcpy;
using std::memset;
using std::size_t;
using std::exit;
// }}}

#include <gawkapi.h>
