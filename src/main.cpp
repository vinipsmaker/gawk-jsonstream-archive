// 4MiB was the initial per-document size limit in MongoDB
#define INITIAL_BUFFER_SIZE 4 * 1024 * 1024

extern "C" {
#include <unistd.h>
}

#include <string_view>
#include <charconv>
#include <cassert>
#include <string>

#include <boost/hana.hpp>

#include <trial/protocol/json/partial/skip.hpp>
#include <trial/protocol/json/reader.hpp>

#include <jsonstream/jpat.hpp>

// Must be included before gawkapi {{{
#include <cstdio>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <cstdlib>

extern "C" {
#include <sys/stat.h>
}

using std::FILE;
using std::memcpy;
using std::memset;
using std::size_t;
using std::exit;
// }}}

#include <gawkapi.h>

namespace hana = boost::hana;
namespace json = trial::protocol::json;

static_assert(GAWK_API_MAJOR_VERSION == 3 && GAWK_API_MINOR_VERSION >= 0,
              "This plug-in has been developed against documentation for gawk "
              "5.1.0. Please update your dist.");

// used in gawk macros {{{
const gawk_api_t *api;
awk_ext_id_t ext_id;
static const char *ext_version = "jsonstream extension: version 0.1";
// }}}

int plugin_is_GPL_compatible;

static awk_bool_t can_take_file(const awk_input_buf_t* iobuf);
static awk_bool_t take_control_of(awk_input_buf_t* iobuf);

awk_array_t var_jpat;
awk_scalar_t var_junwind;
static awk_array_t var_j;
static awk_input_parser_t parser = {
    /*name=*/"jsonstream",
    can_take_file,
    take_control_of,
    /*next=*/NULL //< gawk internal use
};
static const awk_fieldwidth_info_t zero_fields = {
    /*use_chars=*/awk_false,
    /*nf=*/0,
    /*fields=*/{}
};
static std::string buffer;
static std::string::size_type buffer_used;

constexpr auto max_digits = hana::second(hana::while_(
    [](auto s) { return hana::first(s) > hana::size_c<0>; },
    hana::make_pair(
        /*i=*/hana::size_c<std::numeric_limits<size_t>::max()>,
        /*max_digits=*/hana::size_c<0>),
    [](auto s) {
        return hana::make_pair(
            hana::first(s) / hana::size_c<10>,
            hana::second(s) + hana::size_c<1>);
    }
));

void fill_j(json::reader& reader, Tree& node)
{
    // Maybe the code would get more elegant if the branch to explicitly handle
    // atom values at root level were moved out of this recursive function.

    auto consume_value = [&](size_t idx) {
        switch (reader.symbol()) {
        case json::token::symbol::end:
        case json::token::symbol::error:
        case json::token::symbol::end_array:
        case json::token::symbol::end_object:
            assert(false);
        case json::token::symbol::boolean: {
            awk_value_t idx_as_val;
            awk_value_t value;
            set_array_element(
                var_j,
                make_number(idx + 1, &idx_as_val),
                make_number(reader.value<bool>() ? 1 : 0, &value));
        }
            if (false)
        case json::token::symbol::integer:
        case json::token::symbol::real: {
            awk_value_t idx_as_val;
            awk_value_t value;
            set_array_element(
                var_j,
                make_number(idx + 1, &idx_as_val),
                make_number(reader.value<double>(), &value));
        }
            if (false)
        case json::token::symbol::string: {
            awk_value_t idx_as_val;
            awk_value_t value_as_val;
            auto value = reader.value<std::string>();
            char* mem = (char*)gawk_malloc(value.size() + 1);
            memcpy(mem, value.c_str(), value.size() + 1);
            set_array_element(
                var_j,
                make_number(idx + 1, &idx_as_val),
                make_malloced_string(mem, value.size(), &value_as_val));
        }
        case json::token::symbol::null:
            if (!reader.next()) throw json::error{reader.error()};
            break;
        case json::token::symbol::begin_array:
        case json::token::symbol::begin_object:
            json::partial::skip(reader);
        }
    };

    switch (reader.symbol()) {
    case json::token::symbol::error:
        throw json::error{reader.error()};
    case json::token::symbol::begin_object: {
        if (!reader.next()) throw json::error{reader.error()};

        std::string current_key;
        for (;;) {
            if (reader.symbol() == json::token::symbol::end_object) {
                reader.next();
                break;
            }

            // Key
            assert(reader.symbol() == json::token::symbol::string);
            current_key.clear();
            auto ec = reader.string(current_key);
            assert(!ec); (void)ec;

            if (!reader.next()) throw json::error{reader.error()};

            // Value
            auto it = node.find(current_key);
            if (it == node.end()) {
                json::partial::skip(reader);
                continue;
            }
            std::visit(
                hana::overload(
                    [&](Tree& tree) {
                        switch (reader.symbol()) {
                        case json::token::symbol::begin_array:
                        case json::token::symbol::begin_object:
                            fill_j(reader, tree);
                            break;
                        default:
                            json::partial::skip(reader);
                        }
                    },
                    consume_value),
                it->second);
        }

        if (reader.symbol() == json::token::symbol::error)
            throw json::error{reader.error()};
    }
        break;
    case json::token::symbol::begin_array: {
        if (!reader.next()) throw json::error{reader.error()};

        std::array<char, max_digits> current_key;
        for (size_t i = 0 ;; ++i) {
            if (reader.symbol() == json::token::symbol::end_array) {
                reader.next();
                break;
            }

            auto s_size = std::to_chars(current_key.data(),
                                        current_key.data() + current_key.size(),
                                        i).ptr - current_key.data();

            auto it = node.find(std::string_view(current_key.data(), s_size));
            if (it == node.end()) {
                json::partial::skip(reader);
                continue;
            }
            std::visit(
                hana::overload(
                    [&](Tree& tree) {
                        switch (reader.symbol()) {
                        case json::token::symbol::begin_array:
                        case json::token::symbol::begin_object:
                            fill_j(reader, tree);
                            break;
                        default:
                            json::partial::skip(reader);
                        }
                    },
                    consume_value),
                it->second);
        }

        if (reader.symbol() == json::token::symbol::error)
            throw json::error{reader.error()};
    }
        break;
    case json::token::symbol::boolean: {
        awk_value_t key;
        awk_value_t value;
        set_array_element(
            var_j,
            make_number(0, &key),
            make_number(reader.value<bool>() ? 1 : 0, &value));
    }
        if (false)
    case json::token::symbol::integer:
    case json::token::symbol::real: {
        awk_value_t key;
        awk_value_t value;
        set_array_element(
            var_j,
            make_number(0, &key),
            make_number(reader.value<double>(), &value));
    }
        if (false)
    case json::token::symbol::string: {
        awk_value_t key;
        awk_value_t value_as_val;
        auto value = reader.value<std::string>();
        char* mem = (char*)gawk_malloc(value.size() + 1);
        memcpy(mem, value.c_str(), value.size() + 1);
        set_array_element(
            var_j,
            make_number(0, &key),
            make_malloced_string(mem, value.size(), &value_as_val));
    }
    case json::token::symbol::null:
        assert(reader.level() == 0);
        reader.next();
        break;
    case json::token::symbol::end:
        assert(reader.level() == 0);
        // RT isn't set on absent records
        break;
    default:
        assert(false);
    }
}

static int get_record(char** out, struct awk_input* iobuf, int* errcode,
                      char** rt_start, size_t* rt_len,
                      const awk_fieldwidth_info_t** field_width)
{
    assert(out != nullptr);
    assert(errcode != nullptr);
    assert(rt_start != nullptr);
    assert(rt_len != nullptr);

    read_jpat();
    clear_array(var_j);

    std::string_view line;

    for (;;) {
        std::string_view buffer_view(buffer.data(), buffer_used);
        auto lf = buffer_view.find('\n');
        if (lf != std::string_view::npos) {
            line = buffer_view.substr(0, lf + 1);
            break;
        }

        if (buffer.size() == buffer_used)
            buffer.resize(buffer.size() * 2);

        ssize_t nread = read(iobuf->fd,
                             const_cast<char*>(buffer.data()) + buffer_used,
                             buffer.size() - buffer_used);
        if (nread == -1) {
            *errcode = errno;
            return EOF;
        }
        if (nread == 0) {
            if (buffer_used == 0)
                return EOF;

            line = std::string_view{buffer.data(), buffer_used};
            break;
        }
        buffer_used += nread;
    }

    json::reader reader{boost::string_view{line.data(), line.size()}};
    try {
        fill_j(reader, tree);
        if (reader.symbol() != json::token::symbol::end)
            throw json::error{reader.error()};
    } catch (const std::exception& e) {
        warning(ext_id, "Error while processing record: %s", e.what());
    }

    // TODO: preserve JSON in the buffer start
    buffer.erase(0, line.size());
    buffer.resize(buffer.size() + line.size());
    buffer_used -= line.size();

    *out = nullptr;
    *rt_len = 0;

    if (field_width != nullptr)
        *field_width = &zero_fields;

    return /*strlen(*out)=*/0;
}

static awk_bool_t can_take_file(const awk_input_buf_t* iobuf)
{
    // TODO: maybe we should also check whether len(JPAT) != 0
    return iobuf->fd != INVALID_HANDLE ? awk_true : awk_false;
}

static awk_bool_t take_control_of(awk_input_buf_t* iobuf)
{
    assert(iobuf->fd != INVALID_HANDLE);
    iobuf->get_record = get_record;
    buffer_used = 0;
    return awk_true;
}

static awk_bool_t init_func()
{
    buffer.resize(INITIAL_BUFFER_SIZE);
    {
        var_jpat = create_array();
        awk_value_t val;
        val.val_type = AWK_ARRAY;
        val.array_cookie = var_jpat;
        if (!sym_update("JPAT", &val)) {
            fatal(ext_id, "Failed to create `JPAT` array");
            return awk_false;
        }
        var_jpat = val.array_cookie;
    }
    {
        awk_value_t value;
        if (!sym_update("JUNWIND", make_number(0, &value))) {
            fatal(ext_id, "Failed to create `JUNWIND` scalar");
            return awk_false;
        }
        awk_bool_t ok = sym_lookup("JUNWIND", AWK_SCALAR, &value);
        assert(ok == awk_true);
        (void)ok;
        var_junwind = value.scalar_cookie;
    }
    {
        var_j = create_array();
        awk_value_t val;
        val.val_type = AWK_ARRAY;
        val.array_cookie = var_j;
        if (!sym_update("J", &val)) {
            fatal(ext_id, "Failed to create `J` array");
            return awk_false;
        }
        var_j = val.array_cookie;
    }

    register_input_parser(&parser);

    return awk_true;
}

static awk_ext_func_t func_table[] = {
    { NULL, NULL, 0, 0, awk_false, NULL }
};

// Uses `api`, `ext_id`, `init_func` and `ext_version`
dl_load_func(func_table, jsonstream, "")
