#pragma once

#include <string_view>
#include <variant>
#include <string>
#include <map>

struct TreeComp
{
    using is_transparent = void;

    bool operator()(const std::string& lhs, const std::string& rhs) const
    {
        return lhs < rhs;
    }

    bool operator()(const std::string_view& lhs, const std::string& rhs) const
    {
        return lhs < rhs;
    }

    bool operator()(const std::string& lhs, const std::string_view& rhs) const
    {
        return lhs < rhs;
    }
};

// the leaf node stores the target J index
struct Tree: std::map<std::string, std::variant<Tree, size_t>, TreeComp>
{};

extern Tree tree;

void read_jpat();
