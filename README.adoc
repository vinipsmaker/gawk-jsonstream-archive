= gawk-jsonstream

:_:

This project aims to bring MongoDB powerful aggregation framework capabilities
to gawk.

NOTE: *However the awk way of doing things must be preserved*. I've seen lots of
projects that carelessly just export C functions to awk and have no concern
about the awk way.

awk is a tabular data processor. Actions that match the pattern are triggered to
work on the fields split for each record. Each line defines a JSON in the likes
of the solutions described at <https://en.wikipedia.org/wiki/JSON_streaming>.
The https://github.com/cesanta/frozen[frozen C library] gave me the initial idea
on how to specify the field split pattern for JSON records to be processed, but
I use https://tools.ietf.org/html/rfc6901[JSON Pointer] for the pattern format.
The `JPAT` array variable holds the paths of interest.

Once a `J[N]` field is changed, requesting the JSON document (through
`json::output()`) will give you the updated version.

As per MongoDB aggregation framework support, this project should not fight
against awk's one-pass mindset. So the following stage operators are not
implemented:

* `$group`
* `$sort`

You can achieve these features by just adding another awk invocation to the
pipeline. A `sort-jsonstream` binary is supplied to make life easier. Grouping
is better handled within an awk script itself.

Indeed many of the stage operators are redundant for awk. Therefore only the
functionality of the `$unwind` operator is provided. It only makes sense to
specify `$unwind` at the same time you specify the split pattern. It'll only
have effect to the next processed records. Just set `JUNWIND` to the index for
which `JPAT` element represents the field that you want to unwind. Setting
`JUNWIND` implicitly enables basic type checking on that field as array
expected.

[source,awk]
----
BEGIN {
    JPAT[1] = "/id"
    JPAT[2] = "/tags"
    JUNWIND = 2
}
----

== `JPAT`

`JPAT` stands for JSON pattern and has purpose similar to gawk's `FPAT`. Each
element in `JPAT` gives a JSON Pointer path for the field of interest. For
instance:

[source,awk]
----
BEGIN {
    JPAT[1] = "/name"
    JPAT[2] = "/pay_rate"
    JPAT[3] = "/hours_worked"
}
J[3] > 0 { print J[1], J[2] * J[3] }
----

Booleans are converted to `1` or `0`, but the original type is preserved. If you
change the field's value to `1` or `0`, the field will stay a boolean. If the
field isn't there or isn't an atom — string, boolean, or number — `J[N]` won't
be `in` `J`.

You can set attributes for each search field. For now, only the `"dom"`
attribute is defined. The following will instruct gawk-jsonstream to not fill
`J[4]` but store a type-preserving DOM instead. Other functions (available in
the `json` namespace this time) must be used to manipulate the DOM.

[source,awk]
----
JPAT[4] = "/extra"
JPAT[4, "dom"] = 1
----

Trying to reference the same field twice in `JPAT` yields an error. The
following will not work:

[source,awk]
----
JPAT[4] = "/extra"
JPAT[5] = "/extra/previous_company"
----

`getline` will throw an error if you try to build such patterns.

== Why?

NoSQL DBs popularity's ranking change wildly as these techies rise and fall, but
MongoDB was one of the most popular in the class of document-oriented DB during
the NoSQL boom. MongoDB will even store documents in the BSON format (a binary
JSON in a nutshell). MongoDB aggregation framework was designed for
batch-processing the whole dataset.

AWK is one of the classic UNIX tools, but one that has a real language beneath
it. This means that you can script behaviour on top of data seen by the
tool. https://arcan-fe.com/2017/10/05/awk-for-realtime-multimedia/[The
consequences of having a real language beneath it are well understood and by
analogy applied to more modern tools such as arcan desktop engine]. AWK was
Perl's precursor and shouldn't need introductions. AWK is very capable for
batch-processing, but has no JSON support.

The project is small, so it won't consume much of my time. gawk already has an
extension system, so we don't need to fork an existing awk interpreter. CLI
tools to process JSON are useful, but currently this field is monopolized by
`jq` which doesn't have a nice language
IMO{_}footnote:[https://news.ycombinator.com/item?id=21981158[``jq``'s syntax is
criticized by many others too.]].

Although small, the project does present a JSON processing challenge. So I think
it does have enough properties — small, it fills a gap in an existing and
established tool, challenging, and not trying to revolutionize anything at all
(it's based on the old'n'boring AWK and it conforms to the tool's mindset of
tabular data processing for streamed records) — to explore how well foundational
JSON libraries perform.

Of course, each particular shadows a set of other properties we could
explore. Particulars from different areas (i.e. other than AWK) would also be of
use to shine insights in the design of foundational JSON libraries. Such new
perspectives must be pursued too.

== Ideas

Attributes to `JPAT` that I could add:

* `"ro"`: Read-only field. When `json::output()` is called, changes to this
  field are ignored.
* `"type"`: Performs basic type checking at JSON parsing time.
* `"schema"`.
